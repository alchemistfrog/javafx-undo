module com.rducreux.undo {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.rducreux.undo to javafx.fxml;
    exports com.rducreux.undo;
}