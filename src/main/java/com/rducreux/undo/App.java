package com.rducreux.undo;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Stack;

public class App extends Application {

    private Stack<UIAction> history = new Stack<>();
    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(new Scene(createContent()));
        stage.show();
    }

    public Parent createContent() {
        VBox root = new VBox();
        TextField field = new TextField();
        Button addBtn = new Button("Add");
        Button undoBtn = new Button("Undo");

        addBtn.setOnAction(event -> {
            String textToAdd = field.getText();

            if (!textToAdd.isEmpty()) {
                UIAction action = new AddText(textToAdd, root);
                run(action);
                field.setText("");
            }
        });

        undoBtn.setOnAction(event -> undo());

        root.getChildren().addAll(field, addBtn, undoBtn);
        root.setPrefSize(500, 500);

        return root;
    }

    private void run(UIAction action) {
        action.run();
        history.add(action);
    }

    private void undo() {
        if (history.empty())
            return;

        UIAction lastAction = history.pop();
        lastAction.undo();
    }

    public static void main(String[] args) {
        launch();
    }
}
