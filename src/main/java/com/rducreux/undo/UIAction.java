package com.rducreux.undo;

public interface UIAction {
    public void run();
    public void undo();
}
