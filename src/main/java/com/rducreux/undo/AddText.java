package com.rducreux.undo;

import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public class AddText implements UIAction{

    private String text;
    private Pane root;
    private Text uiText;

    public AddText(String text, Pane root) {
        this.text = text;
        this.root = root;
    }

    @Override
    public void run() {
        uiText = new Text(text);

        root.getChildren().add(uiText);
    }

    @Override
    public void undo() {
        root.getChildren().remove(uiText);
    }
}
